package main

import (
	"fmt"
	"os"
	"time"
	"tutorial/part-1/packages/manager"
	"tutorial/part-1/packages/server"

	authRoute "tutorial/part-1/api/auth/delivery"
	categoryRoute "tutorial/part-1/api/categories/delivery"
)

func run() error {
	mgr, err := manager.NewInit()
	if err != nil {
		return err
	}

	tzlocation, err := time.LoadLocation(mgr.GetConfig().AppTZ)
	if err != nil {
		return err
	}
	time.Local = tzlocation

	server := server.NewServer(mgr.GetConfig())
	server.Router.Use(mgr.GetMiddleware().InitLog)
	// Routes
	authRoute.NewRoutes(server.Router, mgr)
	categoryRoute.NewRoutes(server.Router, mgr)

	server.RegisterRouter(server.Router)

	return server.ListenAndServer()
}

func main() {
	if err := run(); err != nil {
		fmt.Fprintf(os.Stderr, "%v\n", err)
		os.Exit(1)
	}
}
