package config

import (
	"os"

	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"
)

type ContextKey string

type Config struct {
	AppEnv                    string `mapstructure:"APPENV"`
	AppTZ                     string `mapstructure:"APPTX"`
	AppIsDev                  bool
	DatabaseDriver            string `mapstructure:"RESOURCE_DATABASE_DRIVER"`
	DatabaseMaster            string `mapstructure:"RESOURCE_DATABASE_MASTER_DNS"`
	DatabaseMaxOpenConnection int    `mapstructure:"RESOURCE_DATABASE_MAX_OPEN_CONNECTIONS"`
	DatabaseMaxIdleConnection int    `mapstructure:"RESOURCE_DATABASE_MAX_IDLE_CONNECTIONS"`
	PortHttpServer            string `mapstructure:"SERVER_HTTP_ADDRESS"`
	JwtSingingKey             string `mapstructure:"JWT_SINGNING_KEY"`
	JwtIssuer                 string `mapstructure:"JWT_ISSUER"`
	JwtAccessToken            int    `mapstructure:"JWT_ACCESS_TOKEN_DURATION_SECONDS"`
	JwtRefreshToken           int    `mapstructure:"JWT_REFRESH_TOKEN_DURATION_SECONDS"`
}

func NewConfig() (*Config, error) {
	// if err := godotenv.Load(".env"); err != nil {
	// 	log.Error().Err(err).Msg("[NewConfig-1] Failed To Read Config")
	// 	return nil, err
	// }

	env := os.Getenv("APPENV")
	if env == "" {
		env = "local"
	}

	viper.AddConfigPath(".")
	viper.AddConfigPath("packages/config")
	viper.SetConfigName(".env")
	viper.SetConfigType("env")
	viper.AutomaticEnv()

	if err := viper.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			viper.SetConfigName("placeholder")

			if err := viper.ReadInConfig(); err != nil {
				log.Error().Err(err).Msg("[NewConfig-1] Failed To Read Config")
				return nil, err
			}
		} else {
			log.Error().Err(err).Msg("[NewConfig-2] Failed To Read Config")
			return nil, err
		}
	}

	cfg := &Config{}
	if err := viper.Unmarshal(&cfg); err != nil {
		log.Error().Err(err).Msg("[NewConfig-3] Failed To Unmarshal Config")
		return nil, err
	}

	cfg.AppIsDev = cfg.AppEnv == "staging" || cfg.AppEnv == "local" || cfg.AppEnv == "dev"

	return cfg, nil
}
