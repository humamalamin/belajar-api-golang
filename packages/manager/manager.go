package manager

import (
	"os"
	paginationHelper "tutorial/part-1/helper/pagination"
	jwtAuth "tutorial/part-1/packages/auth/jwt"
	middlewareAuth "tutorial/part-1/packages/auth/middleware"
	"tutorial/part-1/packages/config"
	gormDatabase "tutorial/part-1/packages/database"
	"tutorial/part-1/packages/server"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"gorm.io/gorm"
)

type Manager interface {
	GetConfig() *config.Config
	GetServer() *server.Server
	GetDatabase() *gorm.DB
	GetJwt() jwtAuth.Jwt
	GetMiddleware() middlewareAuth.Middleware
	GetPagination() paginationHelper.Pagination
}

type manager struct {
	config     *config.Config
	server     *server.Server
	database   *gorm.DB
	jwtAuth    jwtAuth.Jwt
	middleware middlewareAuth.Middleware
	pagination paginationHelper.Pagination
}

func NewInit() (Manager, error) {
	cfg, err := config.NewConfig()
	if err != nil {
		log.Error().Err(err).Msg("[NewInit-1] Failed to Initialize Configuration")
		return nil, err
	}

	srv := server.NewServer(cfg)

	dbGorm, err := gormDatabase.NewGorm(cfg).Connect()
	if err != nil {
		log.Error().Err(err).Msg("[NewInit-2] Failed to Initialize Database")
		return nil, err
	}

	jwt := jwtAuth.NewJwt(cfg)
	middlewareAuth := middlewareAuth.NewMiddleware(cfg)

	log.Logger = log.With().Caller().Logger()
	if cfg.AppIsDev {
		log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stdout, TimeFormat: "2006-01-02 15:04:05"}).With().Caller().Logger()
	}

	paginationHelper := paginationHelper.NewPagination()

	return &manager{
		config:     cfg,
		server:     srv,
		database:   dbGorm,
		jwtAuth:    jwt,
		middleware: middlewareAuth,
		pagination: paginationHelper,
	}, nil
}

func (sm *manager) GetConfig() *config.Config {
	return sm.config
}

func (sm *manager) GetServer() *server.Server {
	return sm.server
}

func (sm *manager) GetDatabase() *gorm.DB {
	return sm.database
}

func (sm *manager) GetJwt() jwtAuth.Jwt {
	return sm.jwtAuth
}

func (sm *manager) GetMiddleware() middlewareAuth.Middleware {
	return sm.middleware
}

func (sm *manager) GetPagination() paginationHelper.Pagination {
	return sm.pagination
}
