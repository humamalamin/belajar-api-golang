package jwtAuth

import "github.com/golang-jwt/jwt"

type JwtData struct {
	UserID   float64 `json:"uid"`
	UserName string  `json:"uname"`
	jwt.StandardClaims
}
