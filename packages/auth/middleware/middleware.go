package middlewareAuth

import (
	"bytes"
	"context"
	"errors"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	jsonHelper "tutorial/part-1/helper/json"
	jwtAuth "tutorial/part-1/packages/auth/jwt"
	"tutorial/part-1/packages/config"

	"github.com/rs/zerolog/log"
)

type Middleware interface {
	InitLog(next http.Handler) http.Handler
	CheckToken(next http.Handler) http.Handler
	GetUserInfoFromContext(ctx context.Context) (*UserData, error)
}

type Options struct {
	jwt jwtAuth.Jwt
}

func (o *Options) getTokeninHeader(r *http.Request) (string, error) {
	authHeader := r.Header.Get("Authorization")
	if authHeader == "" {
		return "", ErrorAuthHeaderEmpty
	}

	accessToken := strings.Split(authHeader, " ")
	if accessToken[0] != "Bearer" {
		return "", ErrorAuthNotHaveBearer
	}

	if len(accessToken) == 1 {
		return "", ErrorAuthNotHaveToken
	}

	return accessToken[1], nil
}

// CheckToken implements Middleware
func (o *Options) CheckToken(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		accessToken, err := o.getTokeninHeader(r)
		if err != nil {
			code := "[Middleware] Middleware - 1"
			log.Error().Err(err).Msg(code)
			jsonHelper.ErrorResponse(w, r, false, http.StatusUnauthorized, "4002", nil, err.Error())
			return
		}

		jwtData, err := o.jwt.VerifyAccessToken(accessToken)
		if err != nil {
			code := "[Middleware] Middleware - 2"
			if err.Error() == "Token is expired" {
				err = errors.New("token is expired")
				log.Error().Err(err).Msg(code)
				jsonHelper.ErrorResponse(w, r, false, http.StatusUnauthorized, "4002", nil, err.Error())
				return
			}

			log.Error().Err(err).Msg(code)
			jsonHelper.ErrorResponse(w, r, false, http.StatusUnauthorized, "4002", nil, err.Error())
			return
		}

		if jwtData == nil {
			code := "[Middleware] Middleware - 3"
			log.Error().Err(err).Msg(code)
			jsonHelper.ErrorResponse(w, r, false, http.StatusUnauthorized, "4004", nil, ErrorUserTokenEmpty.Error())
			return
		}

		ctx := context.WithValue(r.Context(), config.ContextKey("userInfo"), jwtData)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

// GetUserInfoFromContext implements Middleware
func (o *Options) GetUserInfoFromContext(ctx context.Context) (*UserData, error) {
	userInfo, ok := ctx.Value(config.ContextKey("userInfo")).(*jwtAuth.JwtData)
	if !ok || userInfo == nil {
		code := "[Middleware] GetUserInfoFromContext - 1"
		log.Error().Err(ErrorUserFromContext).Msg(code)
		return nil, ErrorUserFromContext
	}

	newUserID := userInfo.UserID

	userData := &UserData{
		UserID:   int(newUserID),
		Username: userInfo.UserName,
	}

	return userData, nil
}

// InitLog implements Middleware
func (o *Options) InitLog(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		bodyBytes, _ := ioutil.ReadAll(r.Body)
		r.Body.Close()
		r.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))

		tnow := time.Now()

		ctx := context.WithValue(r.Context(), config.ContextKey("body"), bodyBytes)
		ctx = context.WithValue(ctx, config.ContextKey("startime"), tnow)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func NewMiddleware(cfg *config.Config) Middleware {
	opt := new(Options)
	opt.jwt = jwtAuth.NewJwt(cfg)

	return opt
}
