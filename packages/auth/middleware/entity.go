package middlewareAuth

type UserData struct {
	UserID   int    `json:"uid"`
	Username string `json:"uname"`
}
