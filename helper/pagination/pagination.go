package paginationHelper

import (
	"errors"
	"math"
	"strconv"
	validationHelper "tutorial/part-1/helper/validation"
)

type Pagination interface {
	AddPagination(totalData int, page string, perPage string) (*Page, error)
}

type Options struct{}

// AddPagination implements Pagination
func (pagination *Options) AddPagination(totalData int, page string, perPage string) (*Page, error) {
	if page == "" {
		return nil, errors.New("6001")
	}

	err := validationHelper.StrIsDigit(page)
	if err != nil {
		return nil, errors.New("6002")
	}

	newPage, _ := strconv.Atoi(page)

	if newPage <= 0 {
		return nil, errors.New("6003")
	}

	limitData := 10
	if perPage != "" {
		limitData, _ = strconv.Atoi(perPage)
	}

	totalPage := int(math.Ceil(float64(totalData) / float64(limitData)))

	last := (newPage * limitData)
	first := last - limitData

	if totalData < last {
		last = totalData
	}

	zeroPage := &Page{PageCount: 1, Page: newPage}
	if totalPage == 0 && newPage == 1 {
		return zeroPage, nil
	}

	if newPage > totalPage {
		return nil, errors.New("6004")
	}

	pages := &Page{
		Page:       newPage,
		PerPage:    limitData,
		PageCount:  totalPage,
		TotalCount: totalData,
		First:      first,
		Last:       last,
	}

	return pages, nil
}

func NewPagination() Pagination {
	pagination := new(Options)

	return pagination
}
