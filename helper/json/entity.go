package jsonHelper

import paginationHelper "tutorial/part-1/helper/pagination"

type meta struct {
	Status     bool                   `json:"status"`
	Code       string                 `json:"code"`
	Message    interface{}            `json:"message"`
	Pagination *paginationHelper.Page `json:"pagination,omitempty"`
}

type response struct {
	Meta meta        `json:"meta"`
	Data interface{} `json:"data"`
}
