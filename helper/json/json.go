package jsonHelper

import (
	"encoding/json"
	"net/http"

	logHelper "tutorial/part-1/helper/log"
	paginationHelper "tutorial/part-1/helper/pagination"
)

func WriteJSON(w http.ResponseWriter, code int, v interface{}) error {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	enc := json.NewEncoder(w)
	enc.SetEscapeHTML(false)
	return enc.Encode(v)
}

func SuccessResponse(w http.ResponseWriter, r *http.Request, status bool, httpStatus int, code string, data interface{}, message string, pagination *paginationHelper.Page) {
	meta := &meta{
		Status:     status,
		Code:       code,
		Message:    message,
		Pagination: pagination,
	}

	res := &response{
		Meta: *meta,
		Data: data,
	}

	logHelper.AddLog(r, "SUCCESS", data)

	WriteJSON(w, httpStatus, res)
}

func ErrorResponse(w http.ResponseWriter, r *http.Request, status bool, httpStatus int, code string, data interface{}, message string) {
	meta := &meta{
		Status:  status,
		Code:    code,
		Message: message,
	}

	res := &response{
		Meta: *meta,
		Data: data,
	}

	logHelper.AddLog(r, "ERROR", data)

	WriteJSON(w, httpStatus, res)
}
