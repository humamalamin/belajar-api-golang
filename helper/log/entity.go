package logHelper

import "time"

type ResponseLog struct {
	HostName      string                 `json:"hostname"`
	Path          string                 `json:"path"`
	RequestMethod string                 `json:"reuest_method"`
	Params        map[string]interface{} `json:"params"`
	Body          map[string]interface{} `json:"body"`
	Headers       map[string]interface{} `json:"headers"`
	UserAgent     string                 `json:"user_agent"`
	UserID        int64                  `json:"article_id"`
	ResponseTime  time.Duration          `json:"response_time"`
	Response      interface{}            `json:"response"`
}
