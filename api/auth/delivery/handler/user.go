package handlerAuth

import (
	"encoding/json"
	"errors"
	"net/http"
	"strconv"
	entityDomain "tutorial/part-1/api/auth/domain/entity"
	interfaceDomain "tutorial/part-1/api/auth/domain/interface"
	usecaseAuth "tutorial/part-1/api/auth/usecase"
	jsonHelper "tutorial/part-1/helper/json"
	"tutorial/part-1/packages/manager"

	"github.com/rs/zerolog/log"
)

type User struct {
	Usecase interfaceDomain.UserUsecase
}

// Login implements interfaceDomain.UserHandler
func (h *User) Login() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var req *entityDomain.User
		if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
			code := "[Handler] Login - 1"
			log.Error().Err(err).Msg(code)
			jsonHelper.ErrorResponse(w, r, false, http.StatusBadRequest, "4000", nil, err.Error())
			return
		}

		err := req.ValidateUser()
		if err != nil {
			code := "[Handler] Login - 2"
			log.Error().Err(err).Msg(code)
			jsonHelper.ErrorResponse(w, r, false, http.StatusBadRequest, "4001", nil, err.Error())
			return
		}

		dataUser, err := h.Usecase.Login(r.Context(), req)
		if err != nil {
			code := "[Handler] Login - 3"
			log.Error().Err(err).Msg(code)
			jsonHelper.ErrorResponse(w, r, false, http.StatusBadRequest, "4003", nil, err.Error())
			return
		}

		newReq, err := h.Usecase.GenerateToken(r.Context(), dataUser.ID)
		if err != nil {
			code := "[Handler] Login - 4"
			log.Error().Err(err).Msg(code)
			jsonHelper.ErrorResponse(w, r, false, http.StatusBadRequest, "4003", nil, err.Error())
			return
		}

		jsonHelper.SuccessResponse(w, r, true, http.StatusOK, "1000", newReq, "Login success", nil)
	})
}

// RefreshToken implements interfaceDomain.UserHandler
func (h *User) RefreshToken() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var req *entityDomain.Token
		if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
			code := "[Handler] RefreshToken - 1"
			log.Error().Err(err).Msg(code)
			jsonHelper.ErrorResponse(w, r, false, http.StatusBadRequest, "4000", nil, err.Error())
			return
		}

		err := req.ValidateToken()
		if err != nil {
			code := "[Handler] RefreshToken - 2"
			log.Error().Err(err).Msg(code)
			jsonHelper.ErrorResponse(w, r, false, http.StatusBadRequest, "4001", nil, err.Error())
			return
		}

		jwtID, err := h.Usecase.VerifyRefreshToken(r.Context(), *req)
		if err != nil {
			code := "[Handler] RefreshToken - 3"
			if err.Error() == "Token is expired" {
				err = errors.New("token is expired")
				log.Error().Err(err).Msg(code)
				jsonHelper.ErrorResponse(w, r, false, http.StatusUnauthorized, "4002", nil, err.Error())
				return
			}
			log.Error().Err(err).Msg(code)
			jsonHelper.ErrorResponse(w, r, false, http.StatusBadRequest, "4002", nil, err.Error())
			return
		}

		jwtIDInt, _ := strconv.Atoi(jwtID)

		newReq, err := h.Usecase.GenerateToken(r.Context(), jwtIDInt)
		if err != nil {
			code := "[Handler] RefreshToken - 4"
			log.Error().Err(err).Msg(code)
			jsonHelper.ErrorResponse(w, r, false, http.StatusBadRequest, "4003", nil, err.Error())
			return
		}

		jsonHelper.SuccessResponse(w, r, true, http.StatusOK, "1000", newReq, "Refresh token success", nil)
	})
}

func NewUserHandler(mgr manager.Manager) interfaceDomain.UserHandler {
	handler := new(User)
	handler.Usecase = usecaseAuth.NewUserUsecase(mgr)

	return handler
}
