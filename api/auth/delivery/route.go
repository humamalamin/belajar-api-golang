package routesAuth

import (
	"tutorial/part-1/packages/manager"

	"github.com/gorilla/mux"

	authRoute "tutorial/part-1/api/auth/delivery/routes"
)

func NewRoutes(r *mux.Router, mgr manager.Manager) {
	apiAuth := r.PathPrefix("/auth").Subrouter()

	authRoute.NewUserRoute(mgr, apiAuth)
}
