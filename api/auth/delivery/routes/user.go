package routesAuth

import (
	"tutorial/part-1/packages/manager"

	"github.com/gorilla/mux"

	userHandler "tutorial/part-1/api/auth/delivery/handler"
)

func NewUserRoute(mgr manager.Manager, route *mux.Router) {
	UserHandler := userHandler.NewUserHandler(mgr)

	route.Handle("/login", UserHandler.Login()).Methods("POST")
	route.Handle("/access-token/refresh", UserHandler.RefreshToken()).Methods("POST")
}
