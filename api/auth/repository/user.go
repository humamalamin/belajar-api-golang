package repositoryAuth

import (
	"context"
	"fmt"
	entityDomain "tutorial/part-1/api/auth/domain/entity"
	interfaceDomain "tutorial/part-1/api/auth/domain/interface"

	"github.com/rs/zerolog/log"
	"gorm.io/gorm"
)

type user struct {
	DB *gorm.DB
}

// GetUserByID implements interfaceDomain.UserRepository
func (repo *user) GetUserByID(ctx context.Context, userID int) (*entityDomain.DataInToken, error) {
	var count int64
	var data *entityDomain.DataInToken
	err := repo.DB.Table("users").Select("id as uid", "name as uname").
		Where("id = ?", userID).Find(&data).Count(&count).Error

	if err != nil {
		code := "[Repository] GetUserByID - 1"
		log.Error().Err(err).Msg(code)
		return nil, err
	}

	if count < 1 {
		code := "[Repository] GetUserByID - 2"
		err = fmt.Errorf("%s with identifier %d doesn't exists", "User", userID)
		log.Error().Err(err).Msg(code)
		return nil, err
	}

	return data, nil
}

// Login implements interfaceDomain.UserRepository
func (repo *user) Login(ctx context.Context, req *entityDomain.User) (*entityDomain.ResultUser, error) {
	result := &entityDomain.ResultUser{}
	err := repo.DB.Table("users").
		Where("email = ?", req.Email).Find(&result).Error

	if err != nil {
		code := "[Repository] Login - 1"
		log.Error().Err(err).Msg(code)
		return nil, err
	}

	if result == nil {
		code := "[Repository] Login - 2"
		err = fmt.Errorf("%s with identifier %s doesn't exists", "User", req.Email)
		log.Error().Err(err).Msg(code)
		return nil, err
	}

	return result, nil
}

func NewUserRepository(database *gorm.DB) interfaceDomain.UserRepository {
	repo := new(user)
	repo.DB = database

	return repo
}
