package interfaceDomain

import (
	"context"
	"net/http"
	entityDomain "tutorial/part-1/api/auth/domain/entity"
)

type UserHandler interface {
	Login() http.Handler
	RefreshToken() http.Handler
}

type UserUsecase interface {
	Login(ctx context.Context, req *entityDomain.User) (*entityDomain.ResultUser, error)
	GenerateToken(ctx context.Context, userID int) (*entityDomain.Token, error)
	VerifyRefreshToken(ctx context.Context, req entityDomain.Token) (string, error)
}

type UserRepository interface {
	Login(ctx context.Context, req *entityDomain.User) (*entityDomain.ResultUser, error)
	GetUserByID(ctx context.Context, userID int) (*entityDomain.DataInToken, error)
}
