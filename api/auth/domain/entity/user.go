package entityDomain

import "fmt"

type Token struct {
	AccessToken  string `json:"access_token"`
	RefreshToken string `json:"refresh_token"`
}

type DataInToken struct {
	UserID int    `json:"uid" gorm:"column:uid"`
	Name   string `json:"uname" gorm:"column:uname"`
}

type User struct {
	Email    string `json:"email" gorm:"email"`
	Password string `json:"password" gorm:"password"`
}

type ResultUser struct {
	Email    string `json:"email"`
	Password string `json:"password"`
	ID       int    `json:"id" gorm:"id"`
	Name     string `json:"name"`
}

func (o *Token) ValidateToken() error {
	if o.RefreshToken == "" {
		return fmt.Errorf("%s field cannot be empty", "refresh_token")
	}

	return nil
}

func (o *User) ValidateUser() error {
	if o.Email == "" {
		return fmt.Errorf("%s field cannot be empty", "email")
	}

	if o.Password == "" {
		return fmt.Errorf("%s field cannot be empty", "password")
	}

	return nil
}
