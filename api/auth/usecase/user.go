package usecaseAuth

import (
	"context"
	"errors"
	entityDomain "tutorial/part-1/api/auth/domain/entity"
	interfaceDomain "tutorial/part-1/api/auth/domain/interface"
	repositoryAuth "tutorial/part-1/api/auth/repository"
	conversionHelper "tutorial/part-1/helper/conversion"
	jwtAuth "tutorial/part-1/packages/auth/jwt"
	"tutorial/part-1/packages/config"
	"tutorial/part-1/packages/manager"

	"github.com/golang-jwt/jwt"
	"github.com/rs/zerolog/log"
)

type User struct {
	repo interfaceDomain.UserRepository
	jwt  jwtAuth.Jwt
	cfg  *config.Config
}

// GenerateToken implements interfaceDomain.UserUsecase
func (uc *User) GenerateToken(ctx context.Context, userID int) (*entityDomain.Token, error) {
	data, err := uc.repo.GetUserByID(ctx, userID)
	if err != nil {
		code := "[Usecase] GenerateToken - 1"
		log.Error().Err(err).Msg(code)
		return nil, err
	}

	jwtData := &jwtAuth.JwtData{
		UserID:   float64(data.UserID),
		UserName: data.Name,
		StandardClaims: jwt.StandardClaims{
			Id:        string(userID),
			NotBefore: jwt.TimeFunc().Local().Unix(),
		},
	}

	accessToken, refreshToken, err := uc.jwt.GenerateToken(jwtData)
	if err != nil {
		code := "[Usecase] GenerateToken - 2"
		log.Error().Err(err).Msg(code)
		return nil, err
	}

	token := &entityDomain.Token{
		AccessToken:  accessToken,
		RefreshToken: refreshToken,
	}

	return token, nil
}

// Login implements interfaceDomain.UserUsecase
func (uc *User) Login(ctx context.Context, req *entityDomain.User) (*entityDomain.ResultUser, error) {
	dataUser, err := uc.repo.Login(ctx, req)
	if err != nil {
		code := "[Usecase] Login - 1"
		log.Error().Err(err).Msg(code)
		return nil, err
	}

	hash, _ := conversionHelper.HashPassword(req.Password)
	match := conversionHelper.CheckPasswordHash(dataUser.Password, hash)
	if !match {
		code := "[Usecase] Login - 2"
		log.Error().Err(err).Msg(code)
		err = errors.New("Password not match!")
		return nil, err
	}

	return dataUser, nil
}

// VerifyRefreshToken implements interfaceDomain.UserUsecase
func (uc *User) VerifyRefreshToken(ctx context.Context, req entityDomain.Token) (string, error) {
	jwtID, err := uc.jwt.VerifyRefreshToken(req.RefreshToken)
	if err != nil {
		code := "[Usecase] VerifyRefreshToken - 1"
		log.Error().Err(err).Msg(code)
		return "", err
	}

	return jwtID, nil
}

func NewUserUsecase(mgr manager.Manager) interfaceDomain.UserUsecase {
	usecase := new(User)
	usecase.repo = repositoryAuth.NewUserRepository(mgr.GetDatabase())
	usecase.cfg = mgr.GetConfig()
	usecase.jwt = mgr.GetJwt()

	return usecase
}
