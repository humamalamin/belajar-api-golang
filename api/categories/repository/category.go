package repositoryCategory

import (
	"context"
	entityDomainCategory "tutorial/part-1/api/categories/domain/entity"
	interfaceDomainCategory "tutorial/part-1/api/categories/domain/interface"

	"github.com/rs/zerolog/log"
	"gorm.io/gorm"
)

type category struct {
	DB *gorm.DB
}

// GetAll implements interfaceDomainCategory.CategoryRepository
func (repo *category) GetAll(ctx context.Context) ([]entityDomainCategory.Category, error) {
	results := []entityDomainCategory.Category{}

	err := repo.DB.Find(&results).Error
	if err != nil {
		code := "[Repository] Category - 1"
		log.Error().Err(err).Msg(code)
		return nil, err
	}

	return results, nil
}

func NewCategoryRepository(database *gorm.DB) interfaceDomainCategory.CategoryRepository {
	repo := new(category)
	repo.DB = database

	return repo
}
