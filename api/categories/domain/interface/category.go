package interfaceDomainCategory

import (
	"context"
	"net/http"
	entityDomainCategory "tutorial/part-1/api/categories/domain/entity"
)

type CategoryHandler interface {
	GetAll() http.Handler
}

type CategoryUsecase interface {
	GetAll(ctx context.Context) ([]entityDomainCategory.Category, error)
}

type CategoryRepository interface {
	GetAll(ctx context.Context) ([]entityDomainCategory.Category, error)
}
