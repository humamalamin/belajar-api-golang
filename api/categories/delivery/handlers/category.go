package handlersCategoryDelivery

import (
	"net/http"
	interfaceDomainCategory "tutorial/part-1/api/categories/domain/interface"
	usecaseCategory "tutorial/part-1/api/categories/usecase"
	jsonHelper "tutorial/part-1/helper/json"
	paginationHelper "tutorial/part-1/helper/pagination"
	middlewareAuth "tutorial/part-1/packages/auth/middleware"
	"tutorial/part-1/packages/manager"

	"github.com/rs/zerolog/log"
)

type Category struct {
	Usecase    interfaceDomainCategory.CategoryUsecase
	Pagination paginationHelper.Pagination
	Middleware middlewareAuth.Middleware
}

// GetAll implements interfaceDomainCategory.CategoryHandler
func (h *Category) GetAll() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()

		_, err := h.Middleware.GetUserInfoFromContext(ctx)
		if err != nil {
			code := "[Handler] GetAll - 1"
			log.Error().Err(err).Msg(code)
			jsonHelper.ErrorResponse(w, r, false, http.StatusUnauthorized, "4002", nil, err.Error())
			return
		}

		page := r.FormValue("page")
		if page == "" {
			page = "1"
		}

		perPage := r.FormValue("per_page")
		if perPage == "" {
			perPage = "10"
		}

		results, err := h.Usecase.GetAll(ctx)
		if err != nil {
			code := "[Handler] GetAll - 2"
			log.Error().Err(err).Msg(code)
			jsonHelper.ErrorResponse(w, r, false, http.StatusBadRequest, "4003", nil, err.Error())
			return
		}

		pagination, err := h.Pagination.AddPagination(len(results), page, perPage)
		if err != nil {
			code := "[Handler] GetAll - 3"
			log.Error().Err(err).Msg(code)
			jsonHelper.ErrorResponse(w, r, false, http.StatusBadRequest, "4005", nil, err.Error())
			return
		}

		jsonHelper.SuccessResponse(w, r, true, http.StatusOK, "1000", results[pagination.First:pagination.Last], "success", pagination)
	})
}

func NewCategoryHandler(mgr manager.Manager) interfaceDomainCategory.CategoryHandler {
	handler := new(Category)
	handler.Usecase = usecaseCategory.NewCategoryUsecase(mgr)
	handler.Pagination = mgr.GetPagination()
	handler.Middleware = middlewareAuth.NewMiddleware(mgr.GetConfig())

	return handler
}
