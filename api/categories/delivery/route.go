package deliveryCategory

import (
	categoryRoute "tutorial/part-1/api/categories/delivery/routes"
	"tutorial/part-1/packages/manager"

	"github.com/gorilla/mux"
)

func NewRoutes(r *mux.Router, mgr manager.Manager) {
	apiAuth := r.PathPrefix("/categories").Subrouter()

	categoryRoute.NewCategoryRoute(mgr, apiAuth)
}
