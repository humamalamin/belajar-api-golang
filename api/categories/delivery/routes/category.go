package routesCategoryDelivery

import (
	categoryHandler "tutorial/part-1/api/categories/delivery/handlers"
	"tutorial/part-1/packages/manager"

	"github.com/gorilla/mux"
)

func NewCategoryRoute(mgr manager.Manager, route *mux.Router) {
	categoryHandler := categoryHandler.NewCategoryHandler(mgr)
	route.Use(mgr.GetMiddleware().CheckToken)

	route.Handle("", categoryHandler.GetAll()).Methods("GET")
}
