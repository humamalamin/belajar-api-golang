package usecaseCategory

import (
	"context"
	entityDomainCategory "tutorial/part-1/api/categories/domain/entity"
	interfaceDomainCategory "tutorial/part-1/api/categories/domain/interface"
	repositoryCategory "tutorial/part-1/api/categories/repository"
	"tutorial/part-1/packages/config"
	"tutorial/part-1/packages/manager"

	"github.com/rs/zerolog/log"
)

type Category struct {
	repo interfaceDomainCategory.CategoryRepository
	cfg  *config.Config
}

// GetAll implements interfaceDomainCategory.CategoryUsecase
func (uc *Category) GetAll(ctx context.Context) ([]entityDomainCategory.Category, error) {
	results, err := uc.repo.GetAll(ctx)
	if err != nil {
		code := "[Usecase] Category - 1"
		log.Error().Err(err).Msg(code)
		return nil, err
	}

	return results, nil
}

func NewCategoryUsecase(mgr manager.Manager) interfaceDomainCategory.CategoryUsecase {
	usecase := new(Category)
	usecase.repo = repositoryCategory.NewCategoryRepository(mgr.GetDatabase())
	usecase.cfg = mgr.GetConfig()

	return usecase
}
